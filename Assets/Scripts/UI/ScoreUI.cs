using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreUI : MonoBehaviour, IGameStateHandler
{
    [SerializeField] private Player player;

    private TextMeshProUGUI scoreTMP;

    private int score;

    void Awake()
    {
        scoreTMP = GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
        if (player.Score == score)
        {
            return;
        }

        score = player.Score;

        scoreTMP.text = $"����: {score}";
    }

    public void SetInGameState(bool initial)
    {
        gameObject.SetActive(true);
        score = 0;
        scoreTMP.text = $"����: {score}";
    }

    public void SetLoseState(bool initial)
    {
        gameObject.SetActive(false);
    }
}
