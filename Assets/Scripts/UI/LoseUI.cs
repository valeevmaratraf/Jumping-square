using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class LoseUI : MonoBehaviour, IGameStateHandler
{
    [SerializeField] private Player player;

    private TextMeshProUGUI loseTMP;

    public void SetInGameState(bool initial)
    {
        gameObject.SetActive(false);
    }

    public void SetLoseState(bool initial)
    {
        gameObject.SetActive(true);

        if (loseTMP == null)
        {
            loseTMP = GetComponent<TextMeshProUGUI>();
        }

        if (!initial)
        {
            loseTMP.text = string.Format("�� ��������� :(\n��������� ����: {0}\n������� ������/��� ��� ������ ����� ����", player.Score);
        }
        else
        {
            loseTMP.text = "������� ������/��� ��� ������ ����� ����";
        }
    }
}
