using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGameStateHandler
{
    void SetLoseState(bool initial);
    void SetInGameState(bool initial);
}
