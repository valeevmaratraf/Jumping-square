using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Point : MonoBehaviour, ICanBeReusable, IGameStateHandler
{
    [SerializeField] private float rotationSpeed;
    [SerializeField] private float sqrDistanceToPlayer;
    [SerializeField] private float moveToPlayerStrenghtCoeff;

    private Player player;
    private Renderer _renderer;
    private float leftXEdge;

    public bool IsCanReuse => (!_renderer.isVisible && transform.position.x < leftXEdge) ||
                               !gameObject.activeInHierarchy;

    public void SetLoseState(bool initial)
    {
        gameObject.SetActive(false);
    }

    public void SetInGameState(bool initial)
    {
    }

    private void Start()
    {
        leftXEdge = Camera.main.ScreenToWorldPoint(Vector3.zero).x;
        _renderer = GetComponent<Renderer>();
        player = FindObjectOfType<Player>();
    }

    private void Update()
    {
        if (IsCanReuse)
        {
            gameObject.SetActive(false);
        }

        transform.Rotate(Vector3.forward, rotationSpeed * Time.deltaTime);

        var direction = player.transform.position - transform.position;
        var sqrDistance = Vector3.SqrMagnitude(direction);
        if (sqrDistance < sqrDistanceToPlayer)
        {
            Vector3 delta = (direction.normalized * Time.deltaTime * (1 - (sqrDistance / sqrDistanceToPlayer))) * moveToPlayerStrenghtCoeff;
            transform.position += delta ;
        }
    }

    public void OnReuse()
    {
    }
}
