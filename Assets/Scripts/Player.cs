using System;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody2D))]
public class Player : MonoBehaviour, IGameStateHandler
{
    [SerializeField] private float forceCoeff;

    private Rigidbody2D rb;

    private bool isAddForceButtonDown;

    public event Action Loosed;
    public int Score { get; private set; }

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        isAddForceButtonDown = Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.Mouse0);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.TryGetComponent<IObstacle>(out var _))
        {
            Loosed?.Invoke();
        }

        if (other.TryGetComponent<Point>(out var point))
        {
            Score++;
            point.gameObject.SetActive(false);
        }
    }

    private void FixedUpdate()
    {
        if (isAddForceButtonDown)
        {
            rb.AddForce(Vector3.up * forceCoeff);
        }
    }

    public void SetLoseState(bool initial)
    {
        gameObject.SetActive(false);
    }

    public void SetInGameState(bool initial)
    {
        gameObject.SetActive(true);
        var leftEdgePositionOnScreen = new Vector3()
        {
            x = 0,
            y = Screen.height / 2
        };
        var startPosition = Camera.main.ScreenToWorldPoint(leftEdgePositionOnScreen) + Vector3.right * 4;
        startPosition.z = 0;

        transform.position = startPosition;
        Score = 0;
    }
}
