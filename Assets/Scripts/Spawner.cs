using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;

public class Spawner : MonoBehaviour, IGameStateHandler
{
    [SerializeField] private GameObject instancePrefab;
    [SerializeField] private Transform spawnRoot;
    [SerializeField] private float delay;

    [SerializeField] private float horizontalOffset;

    [SerializeField] private GameObject sceneRoot;

    private List<GameObject> existingInstances;

    private void Start()
    {
        existingInstances = new List<GameObject>(32);
    }

    private IEnumerator DelayProcess()
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            CreateObstacle();
        }
    }

    private void CreateObstacle()
    {
        GameObject instanceGO = TryGetFreeReusable();

        if (instanceGO == null)
        {
            instanceGO = Instantiate(instancePrefab, spawnRoot);
            instanceGO.name = string.Format("{0} {1}", instancePrefab.name, existingInstances.Count);
            existingInstances.Add(instanceGO);
        }
        else
        {
            instanceGO.SetActive(true);
            instanceGO.GetComponent<ICanBeReusable>().OnReuse();
        }

        var instanceCollider = instanceGO.GetComponent<Collider2D>();

        do
        {
            var positionOnScreen = new Vector3()
            {
                x = Screen.width,
                y = Random.Range(0, Screen.height)
            };

            Vector3 position = Camera.main.ScreenToWorldPoint(positionOnScreen);
            position.z = 0;

            instanceGO.transform.position = position;
            instanceGO.transform.position += Vector3.right * horizontalOffset;

        } while (CheckForAreaObstruction(instanceCollider));
    }

    private bool CheckForAreaObstruction(Collider2D target)
    {
        var targetBounds = target.bounds;
        targetBounds.center = target.transform.position;

        foreach (var other in sceneRoot.GetComponentsInChildren<Collider2D>())
        {
            var otherBounds = other.bounds;
            otherBounds.center = other.transform.position;

            if (target == other)
            {
                continue;
            }

            if (targetBounds.Intersects(otherBounds))
            {
                return true;
            }
        }

        return false;
    }

    private GameObject TryGetFreeReusable()
    {
        foreach (var existingInstance in existingInstances)
        {
            var reusable = existingInstance.GetComponent<ICanBeReusable>();
            if (reusable.IsCanReuse)
            {
                return existingInstance.gameObject;
            }
        }

        return null;
    }

    public void SetLoseState(bool initial)
    {
        gameObject.SetActive(false);
    }

    public void SetInGameState(bool initial)
    {
        gameObject.SetActive(true);
        StartCoroutine(DelayProcess());
    }
}
