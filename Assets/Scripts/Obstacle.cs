using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Obstacle : MonoBehaviour, IObstacle, IGameStateHandler, ICanBeReusable
{
    [SerializeField] private float moveYMagnitude;
    [SerializeField] private float moveYSpeed;

    private int moveYDirectionSign;
    private float startPositionY;

    private float MaxY => startPositionY + moveYMagnitude / 2;
    private float MinY => startPositionY - moveYMagnitude / 2;

    private Renderer _renderer;
    private float leftXEdge;

    public bool IsCanReuse => !_renderer.isVisible && transform.position.x < leftXEdge;

    public void SetLoseState(bool initial)
    {
        gameObject.SetActive(false);
    }

    public void SetInGameState(bool initial)
    {
    }

    private void Awake()
    {
        _renderer = GetComponent<Renderer>();
    }

    private void Start()
    {
        startPositionY = transform.position.y;
        leftXEdge = Camera.main.ScreenToWorldPoint(Vector3.zero).x;
        do
        {
            moveYDirectionSign = Random.Range(-1, 1);
        }
        while (moveYDirectionSign == 0);
    }

    private void Update()
    {
        if (IsCanReuse)
        {
            gameObject.SetActive(false);
            return;
        }

        if (transform.position.y > MaxY)
        {
            moveYDirectionSign = -1;
        }

        if (transform.position.y < MinY)
        {
            moveYDirectionSign = 1;
        }

        transform.position += Vector3.up * moveYDirectionSign * moveYSpeed * Time.deltaTime;
    }

    public void OnReuse()
    {
        startPositionY = transform.position.y;
    }
}
