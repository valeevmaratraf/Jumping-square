using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState { InGame, Lose }

public class GameStateController : MonoBehaviour
{
    [SerializeField] private GameObject sceneRoot;

    [SerializeField] private Player player;

    public GameState State { get; private set; }

    private void Start()
    {
        player.Loosed += () => SetState(GameState.Lose, false);
        SetState(GameState.Lose, true);
    }

    private void Update()
    {
        if (State == GameState.Lose && (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0)))
        {
            SetState(GameState.InGame, false);
        }
    }

    private void SetState(GameState state, bool initial)
    {
        if (state == State)
        {
            return;
        }

        State = state;

        var restartables = GetRestartables();
        foreach (var obj in restartables)
        {
            switch (state)
            {
                case GameState.InGame: obj.SetInGameState(initial); break;
                case GameState.Lose: obj.SetLoseState(initial); break;
            }
        }
    }

    private IGameStateHandler[] GetRestartables()
    {
        return sceneRoot.GetComponentsInChildren<IGameStateHandler>(true);
    }
}
