using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICanBeReusable
{
    bool IsCanReuse { get; }
    void OnReuse();
}
